module P01.Foo where

foo01 :: Int -> IO ()
foo01 n
  | n < 0 = putStrLn "n is negative"
  | even n = putStrLn "n is positive and even"
  | otherwise = putStrLn "n is positive and odd"

foo02 :: Int -> Int -> IO ()
foo02 a b =
  if even a && even b then
    putStrLn "a and b are even"
  else if even a && odd a then
    putStrLn "a is even and b is odd"
  else if odd a && even b then
    putStrLn "a is odd and b is even"
  else
    putStrLn "a and b are odd"


