module P01.Bar where

import           Control.Monad (when)

bar01 :: Int -> Int -> String
bar01 a b
  | a < 0, b < 0 = "a < 0 and b < 0"
  | 0 <= a, b < 0 = "0 <= a and b < 0"
  | a < 0, 0 <= b = "a < 0 and 0 <= b"
  | otherwise = "otherwise"

bar02 :: Int -> Int -> IO ()
bar02 a b = do
  when (a < 0 && b < 0) $
    putStrLn "a < 0 && b < 0"

  when (0 <= a && b < 0) $
    putStrLn "0 <= a && b < 0"

  when (a < 0 && 0 <= b) $
    putStrLn "a < 0 && 0 <= b"

  when (0 <= a && 0 <= b) $
    putStrLn "0 <= a && 0 <= b"

bar03 :: Int -> IO ()
bar03 n | n < 0 = putStrLn "Got negative number"
bar03 n = print (go n)
  where
    go i =
      if i < 2 then
        i
      else
        go (i - 1) + go (i - 2)
