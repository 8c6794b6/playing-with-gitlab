module Main (main) where

import           P01.Bar
import           P01.Foo

main :: IO ()
main = do
  foo01 (-20)
  foo01 21
  foo02 1 1
  foo02 1 2

  putStrLn $ bar01 (-1) 0
  putStrLn $ bar01 0 (-1)

  bar02 (-1) 0
  bar02 0 (-1)

  bar03 10
